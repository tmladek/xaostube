import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "",
  plugins: [svelte()],
  server: {
    proxy: {
      "/grid.php": "http://localhost:8000",
    },
  },
});
